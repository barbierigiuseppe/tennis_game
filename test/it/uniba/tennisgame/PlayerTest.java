package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Giuseppe",0);
		//Act
		player.incrementScore();
		//Assert
		assertEquals(1, player.getScore());
	}
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Giuseppe",0);
		//Non c'� act perch� non deve incrementare
		
		//Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Giuseppe",0);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("love",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Giuseppe",1);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("fifteen",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Giuseppe",2);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("thirty",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player("Giuseppe",3);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertEquals("forty",scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {
		//Arrange
		Player player = new Player("Giuseppe",-1);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfMoreThanThree() {
		//Arrange
		Player player = new Player("Giuseppe",4);
		//Act
		String scoreAsString = player.getScoreAsString();
		
		//Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeTie() {
		//Arrange
		Player player = new Player("Giuseppe",1);
		Player opponent = new Player("Giovanni",1);
		
		//Act
		boolean tie = player.isTieWith(opponent);
		
		//Assert
		assertTrue(tie);
		
	}
	
	@Test
	public void scoreShouldHaveAtLeastFortyPoints() {
		//Arrange
		Player player = new Player("Giuseppe",3);
		//act
		boolean outcome = player.hasAtLeastFortyPoints();
		//assert
		assertTrue(outcome);

	}
	
	@Test
	public void scoreShouldHaveLessThanFortyPoints() {
		//Arrange
		Player player = new Player("Giuseppe",2);
		//act
		boolean outcome = player.hasLessThanFortyPoints();
		//assert
		assertTrue(outcome);

	}
	
	@Test
	public void scoreShouldHaveMoreThanFortyPoints() {
		//Arrange
		Player player = new Player("Giuseppe",4);
		//act
		boolean outcome = player.hasMoreThanFourtyPoints();
		//assert
		assertTrue(outcome);

	}
	
	@Test
	public void playerShouldHaveOnePointAdvantageOnOpponent() {
		//Arrange
		Player player = new Player("Giuseppe",2);
		Player opponent = new Player("Giovanni",1);
		
		//Act
		boolean outcome = player.hasOnePointAdvantageOn(opponent);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void playerShouldNotHaveOnePointAdvantageOnOpponent() {
		//Arrange
		Player player = new Player("Giuseppe",3);
		Player opponent = new Player("Giovanni",3);
		
		//Act
		boolean outcome = player.hasOnePointAdvantageOn(opponent);
		
		//Assert
		assertFalse(outcome);
		
	}
	
	@Test
	public void playerShouldHaveAtLeastTwoPointAdvOnOpponent() {
		//Arrange
		Player player = new Player("Giuseppe",3);
		Player opponent = new Player("Giovanni",1);
		
		//Act
		boolean outcome = player.hasAtLeastTwoPointsAdvantageOn(opponent);
		
		//Assert
		assertTrue(outcome);
		
	}
	
	@Test
	public void playerShouldNotHaveAtLeastTwoPointAdvOnOpponent() {
		//Arrange
		Player player = new Player("Giuseppe",1);
		Player opponent = new Player("Giovanni",3);
		
		//Act
		boolean outcome = player.hasAtLeastTwoPointsAdvantageOn(opponent);
		
		//Assert
		assertFalse(outcome);
		
	}
	
	
}
