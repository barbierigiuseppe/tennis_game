package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenThirty() throws Exception{
		//arrange
		Game game = new Game("Giuseppe","Giovanni");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		//act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		//assert
		assertEquals("Giuseppe fifteen - Giovanni thirty", status);
	}
	
	//get the name of the two players 
	
}
